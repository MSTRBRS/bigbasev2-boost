# BigBaseV2
A mod menu base for Grand Theft Auto V.
Strictly for educational purposes.

## Features
* ImGui–based user interface
* Unloading at runtime
* Log console
* Script fibers
* Fiber pool
* Access to YSC script globals
* scrProgram–based native hook

## Building
To build BigBaseV2 you need:
* Visual Studio 2019
* [Premake 5.0](https://premake.github.io/download.html) in your PATH

To set up the build environment, run the following commands in a terminal:
```dos
git clone https://gir489@bitbucket.org/gir489/bigbasev2-fix.git --recurse-submodules
cd BigBaseV2-fix
GenerateProjects.bat
```
Now, you will be able to open the solution, and simply build it in Visual Studio.

## Why bigbasev2-boost?

This base exacly same with bigbasev2-fix, but...
One thing that can help you in development is Boost libs and features wroten with it inside that base

## In Future

* c++ plugin with Boost.DLL library 
-- this can to help you write ur own c++ api for your menu, which will be helpfull for users who can to use this stuff fully!
* All Arrays stuff realization with Boost.MultiIndex
* Powerfull socket api with Boost.Asio/Boost.Beast
* Config serialization/deserialization with Google.Flatbuffer and a tool to convert .fbs to .json or .json to .fbs

## Credits

git489 and his awesome bigbasev2-fix: https://bitbucket.org/gir489/bigbasev2-fix/
pocakking and his awesome bigbasev2: https://github.com/Pocakking/BigBaseV2
